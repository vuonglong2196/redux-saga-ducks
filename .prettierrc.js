module.exports = {
  // Format the string in quotation marks
  singleQuote: true,
  // Format with a semicolon at the end of the code
  semi: true,
  // Format to prevent the use of tabs and replace them with the use of spacebar
  useTabs: false,
  // indent width is 4 spaces
  tabWidth: 2,
  // object or array key: always put a comma after the value
  trailingComma: 'all',
  // Maximum 80 spaces per line of code
  printWidth: 120,
  // formatting to omit parentheses when arrow function takes one parameter
  arrowParens: 'always',
};
