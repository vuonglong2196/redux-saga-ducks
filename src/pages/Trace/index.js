import React from 'react';
import { Col, Row } from 'antd';

import InfoTrace from './components/InfoTrace';
import HistoryTrace from './components/HistoryTrace';

import './style.scss';

const Trace = () => {
  return (
    <div className="trace-page">
      <div className="page-title">
        <div className="title">Truy vết</div>
      </div>
      <div className="page-content">
        <Row className="row-content">
          <Col span={12}>
            <InfoTrace />
          </Col>
          <Col span={12}>Video</Col>
        </Row>
        <Row className="row-content">
          <Col span={12}>
            <HistoryTrace />
          </Col>
          <Col span={12}>Map</Col>
        </Row>
      </div>
    </div>
  );
};

export default Trace;
