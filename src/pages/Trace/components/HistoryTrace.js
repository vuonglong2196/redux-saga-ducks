import React, { useState } from 'react';
import { Empty, Image } from 'antd';
import CustomTable from '../../../components/common/CustomTable/CustomTable';

import './components.scss';
import CustomPagination from '../../../components/common/CustomPagination/CustomPagination';

const HistoryTrace = () => {
  const [data] = useState([
    {
      STT: 1,
      position: 'Vị trí',
      image: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
      time: '27/09/2021 - 15:05:00',
      accuracy: '80%',
    },
  ]);

  const columns = [
    {
      key: 'STT',
      title: 'STT',
      align: 'center',
      width: '50px',
      dataIndex: 'STT',
    },
    {
      key: 'position',
      title: 'Vị trí',
      sorter: true,
      width: '25%',
      showSorterTooltip: false,
      dataIndex: 'position',
    },
    {
      key: 'image',
      title: 'Ảnh',
      sorter: true,
      width: '20%',
      showSorterTooltip: false,
      dataIndex: 'image',
      render: (text) => <Image src={text} />,
    },
    {
      key: 'time',
      title: 'Thời gian',
      sorter: true,
      showSorterTooltip: false,
      dataIndex: 'time',
    },
    {
      key: 'accuracy',
      title: 'Mô tả',
      sorter: true,
      showSorterTooltip: false,
      dataIndex: 'accuracy',
    },
  ];

  return (
    <div className="history-trace">
      <CustomTable
        columns={columns}
        dataSource={data}
        locale={{
          emptyText: <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="Không có kết quả tìm kiếm." />,
        }}
      />
      <div className="history-trace-pagination">
        <CustomPagination total={20} current={1} pageSize={10} />
      </div>
    </div>
  );
};

export default HistoryTrace;
