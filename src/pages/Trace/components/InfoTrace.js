import React, { useState } from 'react';
import { Col, Row, Form, Image } from 'antd';
import moment from 'moment';
import Label from '../../../components/common/CustomLabel/CustomLabel';
import UploadFile from '../../../components/common/UploadFile/UploadFile';
import CustomDatePicker from '../../../components/common/CustomDatePicker/CustomDatePicker';
import CustomTreeSelect from '../../../components/common/CustomTreeSelect/CustomTreeSelect';
import CustomButton from '../../../components/common/CustomButton/CustomButton';
import { treeData } from '../../../components/constant/constants';

import './components.scss';

const InfoTrace = () => {
  const [form] = Form.useForm();
  const [fileName, setFileName] = useState();

  const onSelectFile = ({ file }) => {
    setFileName(file.name);
  };

  return (
    <div className="info-trace">
      <Row gutter={20}>
        <Col span={16}>
          <Form form={form} initialValues={{ startDate: moment() }} layout="horizontal" autoComplete="off">
            <Row gutter={20} className="info-trace-item">
              <Col span={8}>
                <Label title="Ảnh đối tượng" isRequire />
              </Col>
              <Col span={16}>
                <Form.Item
                  label=""
                  name="image"
                  rules={[{ required: true, message: 'Ảnh đối tượng không được để trống!' }]}
                >
                  <UploadFile onChange={onSelectFile} fileName={fileName} />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={20} className="info-trace-item">
              <Col span={8}>
                <Label title="Thời gian bắt đầu" isRequire />
              </Col>
              <Col span={16}>
                <Form.Item
                  label=""
                  name="startDate"
                  rules={[{ required: true, message: 'Thời gian bắt đầu không được để trống!' }]}
                >
                  <CustomDatePicker />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={20} className="info-trace-item">
              <Col span={8}>
                <Label title="Tên Camera" isRequire />
              </Col>
              <Col span={16}>
                <Form.Item
                  label=""
                  name="listCamera"
                  rules={[{ required: true, message: 'Tên Camera không được để trống!' }]}
                >
                  <CustomTreeSelect treeData={treeData} />
                </Form.Item>
              </Col>
            </Row>
          </Form>
          <div className="info-trace-action">
            <CustomButton text="Truy vết" />
            <CustomButton className="cancel" text="Dừng truy vết" />
          </div>
        </Col>
        <Col span={8}>
          <Image src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png" />
        </Col>
      </Row>
    </div>
  );
};

export default InfoTrace;
