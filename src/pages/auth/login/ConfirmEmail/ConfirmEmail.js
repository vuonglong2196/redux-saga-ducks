import React from 'react';
import { Form, Input } from 'antd';
import i18n from 'i18next';
import '../../../../assets/style/form.scss';
import CustomButton from '../../../../components/common/CustomButton/CustomButton';

const ConfirmEmail = () => {
  const onFinish = () => {};

  const onFinishFailed = () => {};

  return (
    <>
      <Form
        className="auth-form-content"
        name="login"
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label={i18n.t('forgotPassword.email')}
          name="email"
          rules={[
            { required: true, message: i18n.t('forgotPassword.emailBlank') },
            { type: 'email', message: i18n.t('forgotPassword.notValidEmail') },
          ]}
        >
          <Input className="form-control" />
        </Form.Item>

        <Form.Item>
          <CustomButton text={i18n.t('forgotPassword.confirmBtn')} htmlType="submit" className="width100 auth-button" />
        </Form.Item>
      </Form>
    </>
  );
};

export default ConfirmEmail;
