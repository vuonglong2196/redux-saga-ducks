import React from 'react';
import { Form, Input } from 'antd';
import './SignIn.scss';
import i18n from 'i18next';
// import { NavLink } from 'react-router-dom';
import '../../../../assets/style/form.scss';
import CustomButton from '../../../../components/common/CustomButton/CustomButton';
import CustomCheckbox from '../../../../components/common/CustomCheckbox/CustomCheckbox';
// import links from '../../../../components/constant/Routes';
import QRCode from '../../../../assets/img/login/QRcode.png';

const SignIn = () => {
  const onFinish = () => {};

  const onFinishFailed = () => {};

  const language = localStorage.getItem('i18language');

  return (
    <>
      <Form
        className="auth-form-content"
        name="login"
        layout="vertical"
        initialValues={{ remember: false }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label={i18n.t('login.username')}
          name="username"
          rules={[{ required: true, message: i18n.t('login.usernameBlank') }]}
        >
          <Input className="form-control" />
        </Form.Item>

        <Form.Item
          // label={(
          //   <div className="label-password">
          //     <label
          //       htmlFor="login_password"
          //       className="ant-form-item-required"
          //       title={i18n.t('login.password')}
          //     >
          //       {i18n.t('login.password')}
          //     </label>
          //     <NavLink to={links.FORGOT_PASSWORD}>{i18n.t('login.forgotPassword')}</NavLink>
          //   </div>
          // )}
          label={i18n.t('login.password')}
          name="password"
          rules={[{ required: true, message: i18n.t('login.passwordBlank') }]}
        >
          <Input.Password className="form-control" />
        </Form.Item>

        <Form.Item name="remember" valuePropName="checked">
          <CustomCheckbox label={i18n.t('login.remember')} />
        </Form.Item>

        <Form.Item>
          <CustomButton text={i18n.t('login.submit')} htmlType="submit" className="width100 auth-button" />
        </Form.Item>
      </Form>
      <div className="auth-content-download">
        <div className="auth-content-download-item">
          <div>
            <p className={`other_icon google_icon${language === 'en' ? ' en_image' : ''}`} />
            <p className={`other_icon ios_icon${language === 'en' ? ' en_image' : ''}`} />
          </div>
          <p className={`pc_icon${language === 'en' ? ' en_image' : ''}`}>
            <span>{i18n.t('login.downloadPc')}</span>
          </p>
        </div>
        <div className="auth-content-download-item">
          <img src={QRCode} alt="" />
        </div>
      </div>
    </>
  );
};

export default SignIn;
