import React from 'react';
import { Form, Input } from 'antd';
import i18n from 'i18next';
import '../../../../assets/style/form.scss';
import CustomButton from '../../../../components/common/CustomButton/CustomButton';

const ChangePassword = () => {
  const onFinish = () => {};

  const onFinishFailed = () => {};

  return (
    <>
      <Form
        className="auth-form-content"
        name="login"
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        initialValues={{ username: 'test' }}
      >
        <Form.Item
          label={i18n.t('login.username')}
          name="username"
          rules={[{ required: true, message: i18n.t('login.usernameBlank') }]}
        >
          <Input className="form-control" disabled />
        </Form.Item>
        <Form.Item
          label={i18n.t('changePass.oldPass')}
          name="oldPassword"
          rules={[{ required: true, message: i18n.t('forgotPassword.passwordBlank') }]}
        >
          <Input.Password className="form-control" />
        </Form.Item>
        <Form.Item
          label={i18n.t('forgotPassword.newPassword')}
          name="newPassword"
          rules={[{ required: true, message: i18n.t('forgotPassword.passwordBlank') }]}
        >
          <Input.Password className="form-control" />
        </Form.Item>

        <Form.Item
          label={i18n.t('forgotPassword.confirmNewPassword')}
          name="confirmNewPassword"
          rules={[{ required: true, message: i18n.t('forgotPassword.passwordBlank') }]}
        >
          <Input.Password className="form-control" />
        </Form.Item>

        <Form.Item>
          <CustomButton text={i18n.t('forgotPassword.submit')} htmlType="submit" className="width100 auth-button" />
        </Form.Item>
      </Form>
    </>
  );
};

export default ChangePassword;
