import React, { useEffect, useState } from 'react';
import './AuthForm.scss';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import VNFlag from '../../../../assets/img/login/vietnam.svg';
import USAFlag from '../../../../assets/img/login/usa.svg';
import logo from '../../../../assets/img/login/logo.png';
import '../../../../assets/style/form.scss';
import links from '../../../../components/constant/Routes';
import SignIn from '../SignIn/SignIn';
import ForgotPassword from '../ForgotPassword/ForgotPassword';

import { changeLanguage } from '../../../../utils/function';
import ChangePassword from '../ChangePassword/ChangePassword';
import ConfirmEmail from '../ConfirmEmail/ConfirmEmail';
import ConfirmOtp from '../ConfirmOtp/ConfirmOtp';

const AuthForm = ({ location }) => {
  const { t } = useTranslation();
  const [path, setPath] = useState('');
  useEffect(() => {
    if (location.pathname) {
      setPath(location.pathname);
    }
  }, [location]);
  const getTitle = () => {
    let title = '';
    if (path) {
      switch (path) {
        case links.LOGIN:
          title = t('login.title');
          break;
        case links.FORGOT_PASSWORD:
          title = t('forgotPassword.title');
          break;
        case links.CONFIRM_EMAIL:
          title = t('forgotPassword.title');
          break;
        case links.CHANGE_PASSWORD:
          title = t('changePass.title');
          break;
        case links.CONFIRM_OTP:
          title = t('confirmOtp.title');
          break;
        default:
          break;
      }
    }
    return title;
  };
  return (
    <div className="auth-content">
      <div className="auth-content-form">
        <div className="auth-content-flag">
          <div
            onClick={() => {
              changeLanguage('vi');
            }}
            aria-hidden
          >
            <img src={VNFlag} alt="Vietnam" />
          </div>
          <div
            onClick={() => {
              changeLanguage('en');
            }}
            aria-hidden
          >
            <img src={USAFlag} alt="USA" />
          </div>
        </div>
        <div className="auth-content-logo">
          <img src={logo} alt="" />
        </div>
        <div className="auth-content-title">{getTitle()}</div>
        {path === links.LOGIN && <SignIn />}
        {path === links.FORGOT_PASSWORD && <ForgotPassword />}
        {path === links.CHANGE_PASSWORD && <ChangePassword />}
        {path === links.CONFIRM_EMAIL && <ConfirmEmail />}
        {path === links.CONFIRM_OTP && <ConfirmOtp />}
      </div>
      <div className="auth-content-copyright">
        <p>Bản quyền thuộc về Tổng công ty Giải pháp Doanh nghiệp Viettel.</p>
        <p>Phiên bản: 1.5</p>
      </div>
    </div>
  );
};

AuthForm.defaultProps = {
  location: {},
};

AuthForm.propTypes = {
  location: PropTypes.object,
};

export default AuthForm;
