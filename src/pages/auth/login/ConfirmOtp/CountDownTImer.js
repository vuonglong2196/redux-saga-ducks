import React, { useEffect, useState } from 'react';

const CountDownTimer = () => {
  const [minutes, setMinutes] = useState(1);
  const [seconds, setSeconds] = useState(59);
  useEffect(() => {
    const myInterval = setInterval(() => {
      if (seconds > 0) {
        setSeconds(seconds - 1);
      }
      if (seconds === 0) {
        if (minutes === 0) {
          clearInterval(myInterval);
        } else {
          setMinutes(minutes - 1);
          setSeconds(59);
        }
      }
    }, 1000);
    return () => {
      clearInterval(myInterval);
    };
  });

  return (
    <span>
      {minutes === 0 && seconds === 0
        ? (
          <span>
            {' '}
            {minutes}
            :
            {`0${seconds}`}
          </span>
        )
        : (
          <span>
            {' '}
            {minutes}
            :
            {seconds < 10 ? `0${seconds}` : seconds}
          </span>
        )}
    </span>

  );
};

export default CountDownTimer;
