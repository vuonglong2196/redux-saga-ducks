import React from 'react';
import { Form, Input } from 'antd';
import i18n from 'i18next';
import '../../../../assets/style/form.scss';
import CustomButton from '../../../../components/common/CustomButton/CustomButton';
import './ConfirmOtp.scss';
import CountDownTimer from './CountDownTImer';

const ConfirmOtp = () => {
  const onFinish = () => {};

  const onFinishFailed = () => {};

  return (
    <>
      <div className="otp-text">
        <p>
          Mã xác nhận đã được gửi về email
          <span> haxx12@gmail.com</span>
          .
        </p>
        <p>Vui lòng kiểm tra tin nhắn để nhập mã xác nhận.</p>
      </div>
      <Form
        className="auth-form-content otp-form"
        name="login"
        layout="inline"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label={i18n.t('confirmOtp.label')}
          name="code"
          rules={[{ required: true, message: i18n.t('confirmOtp.codeBlank') }]}
        >
          <Input className="form-control" />
        </Form.Item>

        <Form.Item>
          <CustomButton text={i18n.t('confirmOtp.confirmBtn')} htmlType="submit" className="auth-button" />
        </Form.Item>
      </Form>
      <div className="otp-notification">
        <div>
          Mã Code hết hiệu lực sau:
          {' '}
          <CountDownTimer />
          {' '}
          phút
        </div>
        <div>
          <CustomButton text={i18n.t('confirmOtp.resend')} className="auth-button" />
        </div>
      </div>
    </>
  );
};

export default ConfirmOtp;
