import React from 'react';
import { Col, Form, Row } from 'antd';
import CustomButton from '../../../components/common/CustomButton/CustomButton';
import CustomInput from '../../../components/common/CustomInput/CustomInput';
import CustomSelect from '../../../components/common/CustomSelect/CustomSelect';

import { lstTypeObject } from '../constants';

import './components.scss';

const Search = () => (
  <div className="div-search">
    <Form labelCol={{ span: 8 }} wrapperCol={{ span: 16 }} initialValues={{ remember: true }} autoComplete="off">
      <Row gutter={20}>
        <Col span={9}>
          <Form.Item label="Tên đối tượng" name="name">
            <CustomInput placeholder="Tên đối tượng" />
          </Form.Item>
        </Col>
        <Col span={9}>
          <Form.Item label="Loại đối tượng" name="type">
            <CustomSelect placeholder="Loại đối tượng" lstOption={lstTypeObject} />
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item>
            <CustomButton text="Tìm kiếm" />
          </Form.Item>
        </Col>
      </Row>
    </Form>
  </div>
);

export default Search;
