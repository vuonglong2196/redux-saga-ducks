import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import { Col, Form, Image, Row } from 'antd';
import CustomModal from '../../../components/common/CustomModal/CustomModal';
import CustomButton from '../../../components/common/CustomButton/CustomButton';
import Label from '../../../components/common/CustomLabel/CustomLabel';
import CustomInput from '../../../components/common/CustomInput/CustomInput';
import CustomSelect from '../../../components/common/CustomSelect/CustomSelect';
import CustomTextArea from '../../../components/common/CustomTextArea/CustomTextArea';
import UploadFile from '../../../components/common/UploadFile/UploadFile';
import { checkSpecialCharacters } from '../../../utils/function';

import { lstTypeObject } from '../constants';

import './components.scss';

const ModalRegister = (props) => {
  const { t } = useTranslation();
  const { isOpen, onClose, isEdit } = props;

  const [form] = Form.useForm();
  const [fileName, setFileName] = useState();

  const onSubmitForm = () => {
    form.validateFields().then(() => {});
  };

  const onSelectFile = ({ file }) => {
    setFileName(file.name);
  };

  useEffect(() => {
    if (!isOpen) return 0;
    if (isEdit) {
      form.setFieldsValue({
        STT: 1,
        image: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
        username: 'Nguyen Tuan Manh',
        description: 'Nga tu Khuat Duy Tien',
        type: 1,
      });
    }
    return () => form.resetFields();
  }, [isOpen, form, isEdit]);

  return (
    <CustomModal
      width={760}
      title={t('registerFaceRecognition.title')}
      visible={isOpen}
      onCancel={onClose}
      footer={
        <div className="action-modal">
          {/* <CustomButton className="cancel" onClick={onClose} text="Huỷ bỏ" /> */}
          <CustomButton onClick={onSubmitForm} text="Đăng ký" />
        </div>
      }
    >
      <div className="content-modal">
        <Row gutter={20}>
          <Col span={12}>
            <Form form={form} initialValues={{ objectType: 1 }} layout="vertical" autoComplete="off">
              <Label title="Tên đối tượng" isRequire />
              <Form.Item
                label=""
                name="username"
                rules={[
                  { required: true, message: 'Tên đối tượng không được để trống!' },
                  () => ({
                    validator(_, value) {
                      if (value && checkSpecialCharacters(value)) {
                        return Promise.reject(
                          new Error('Tên đối tượng không chưa ký tự đặc biệt (trừ dấu gạch dưới và gạch ngang)!'),
                        );
                      }
                      return Promise.resolve();
                    },
                  }),
                ]}
              >
                <CustomInput placeholder="Tên đối tượng" />
              </Form.Item>

              <Label title="Loại đối tượng" />
              <Form.Item label="" name="type">
                <CustomSelect lstOption={lstTypeObject} placeholder="Loại đối tượng" />
              </Form.Item>

              <Label title="Mô tả" />
              <Form.Item label="" name="description">
                <CustomTextArea />
              </Form.Item>

              <Label title="Ảnh đối tượng" isRequire />
              <Form.Item
                label=""
                name="image"
                rules={[{ required: true, message: 'Ảnh đối tượng không được để trống!' }]}
              >
                <UploadFile onChange={onSelectFile} fileName={fileName} />
              </Form.Item>
            </Form>
          </Col>
          <Col span={12}>
            <Image src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png" />
          </Col>
        </Row>
      </div>
    </CustomModal>
  );
};

ModalRegister.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  isEdit: PropTypes.bool.isRequired,
};

export default ModalRegister;
