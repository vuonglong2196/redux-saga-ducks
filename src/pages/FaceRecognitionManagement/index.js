import React, { useState } from 'react';
import { Empty, Image } from 'antd';

import CustomButton from '../../components/common/CustomButton/CustomButton';
import CustomTooltip from '../../components/common/CustomTooltip/CustomTooltip';
import CustomTable from '../../components/common/CustomTable/CustomTable';
import CustomIcon from '../../components/common/CustomIcon/CustomIcon';
import Search from './components/Search';
import Pagination from '../../components/common/CustomPagination/CustomPagination';
import ModalRegister from './components/ModalRegister';
import ModalDelete from './components/ModalDelete';

import iconFaceId from '../../assets/img/iconTable/faceid.svg';
import iconDelete from '../../assets/img/iconTable/delete.svg';
import iconTrade from '../../assets/img/iconTable/trade.svg';

import './style.scss';

const FaceRecognitionManagement = () => {
  const [data] = useState([
    {
      STT: 1,
      image: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
      username: 'Nguyen Tuan Manh',
      description: 'Nga tu Khuat Duy Tien',
      type: 'Black list',
    },
  ]);
  const [isEdit, setIsEdit] = useState(false);
  const [isOpenRegister, setIsOpenRegister] = useState(false);
  const [isOpenDelete, setIsOpenDelete] = useState(false);

  const columns = [
    {
      key: 'STT',
      title: 'STT',
      align: 'center',
      width: '50px',
      dataIndex: 'STT',
    },
    {
      key: 'image',
      title: 'Ảnh',
      sorter: true,
      width: '20%',
      showSorterTooltip: false,
      dataIndex: 'image',
      render: (text) => <Image src={text} />,
    },
    {
      key: 'username',
      title: 'Tên đối tượng',
      sorter: true,
      showSorterTooltip: false,
      dataIndex: 'username',
    },
    {
      key: 'description',
      title: 'Mô tả',
      sorter: true,
      width: '25%',
      showSorterTooltip: false,
      dataIndex: 'description',
    },
    {
      key: 'type',
      title: 'Loại đối tượng',
      sorter: true,
      showSorterTooltip: false,
      dataIndex: 'type',
    },
    {
      key: 'action',
      width: '160px',
      title: 'Thao tác',
      render: () => (
        <div className="table-action">
          <CustomTooltip title="Cập nhật đối tượng nhận diện khuôn mặt" placement="bottom">
            <div
              onClick={() => {
                setIsOpenRegister(true);
                setIsEdit(true);
              }}
              aria-hidden
            >
              <CustomIcon icon={iconFaceId} />
            </div>
          </CustomTooltip>
          <CustomTooltip title="Xoá đối tượng nhận diện khuôn mặt" placement="bottom">
            <div
              onClick={() => {
                setIsOpenDelete(true);
              }}
              aria-hidden
            >
              <CustomIcon icon={iconDelete} />
            </div>
          </CustomTooltip>
          <CustomTooltip title="Truy vết đối tượng" placement="bottom">
            <div>
              <CustomIcon icon={iconTrade} />
            </div>
          </CustomTooltip>
        </div>
      ),
    },
  ];

  return (
    <div className="face-recog-page">
      <div className="page-title">
        <div className="title">Quản lý nhận diện khuôn mặt</div>
        <div className="btn-feature">
          <CustomTooltip title="Đăng ký nhận diện khuôn mặt" placement="bottomRight">
            <CustomButton
              text="+"
              onClick={() => {
                setIsOpenRegister(true);
                setIsEdit(false);
              }}
            />
          </CustomTooltip>
        </div>
      </div>
      <div className="page-search">
        <Search />
      </div>
      <div className="page-table">
        <CustomTable
          columns={columns}
          dataSource={data}
          locale={{
            emptyText: <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="Không có kết quả tìm kiếm." />,
          }}
        />
      </div>
      <div className="page-pagination">
        <Pagination total={100} current={5} pageSize={10} />
      </div>
      <ModalRegister
        isEdit={isEdit}
        isOpen={isOpenRegister}
        onClose={() => {
          setIsOpenRegister(false);
        }}
      />
      <ModalDelete
        isOpen={isOpenDelete}
        data={data[0]}
        onClose={() => {
          setIsOpenDelete(false);
        }}
      />
    </div>
  );
};

export default FaceRecognitionManagement;
