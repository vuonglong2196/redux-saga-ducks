import React, { useState } from 'react';
import { Empty } from 'antd';

import CustomButton from '../../components/common/CustomButton/CustomButton';
import CustomTooltip from '../../components/common/CustomTooltip/CustomTooltip';
import CustomTable from '../../components/common/CustomTable/CustomTable';
import CustomIcon from '../../components/common/CustomIcon/CustomIcon';
import Search from './components/Search';
import Pagination from '../../components/common/CustomPagination/CustomPagination';
import ModalRegister from './components/ModalRegister';
import ModalDelete from './components/ModalDelete';
import ModalHistory from './components/ModalHistory';

import iconDelete from '../../assets/img/iconTable/delete.svg';
import iconHistory from '../../assets/img/iconCommon/history.svg';

import './style.scss';

const FaceRecognitionManagement = () => {
  const [data] = useState([
    {
      STT: 1,
      username: 'demo_account',
      fullname: 'Nguyễn Văn Tùng',
      email: 'demo@gmail.com',
      phoneNumber: '096999999',
      loginTime: '23/09/2021 - 13:30:30',
    },
  ]);
  const [isEdit, setIsEdit] = useState(false);
  const [isOpenRegister, setIsOpenRegister] = useState(false);
  const [isOpenDelete, setIsOpenDelete] = useState(false);
  const [isOpenHistory, setIsOpenHistory] = useState(false);

  const columns = [
    {
      key: 'STT',
      title: 'STT',
      align: 'center',
      width: '50px',
      dataIndex: 'STT',
    },
    {
      key: 'username',
      title: 'Tên đăng nhập',
      sorter: true,
      width: '15%',
      showSorterTooltip: false,
      dataIndex: 'username',
    },
    {
      key: 'fullname',
      title: 'Tên đầy đủ',
      sorter: true,
      width: '20%',
      showSorterTooltip: false,
      dataIndex: 'fullname',
    },
    {
      key: 'email',
      title: 'Email',
      sorter: true,
      width: '15%',
      showSorterTooltip: false,
      dataIndex: 'email',
    },
    {
      key: 'phoneNumber',
      title: 'Số điện thoại',
      sorter: true,
      width: 115,
      showSorterTooltip: false,
      dataIndex: 'phoneNumber',
    },
    {
      key: 'loginTime',
      title: 'Thời gian đăng nhập gần nhất',
      sorter: true,
      width: 210,
      showSorterTooltip: false,
      dataIndex: 'loginTime',
    },
    {
      key: 'action',
      width: 100,
      title: 'Thao tác',
      render: () => (
        <div className="table-action">
          <CustomTooltip title="Xoá tài khoản" placement="bottom">
            <div onClick={() => setIsOpenDelete(true)} aria-hidden>
              <CustomIcon icon={iconDelete} />
            </div>
          </CustomTooltip>
          <CustomTooltip title="Lịch sử truy cập" placement="bottom">
            <div onClick={() => setIsOpenHistory(true)} aria-hidden>
              <CustomIcon icon={iconHistory} />
            </div>
          </CustomTooltip>
        </div>
      ),
    },
  ];

  return (
    <div className="face-recog-page">
      <div className="page-title">
        <div className="title">Quản lý tài khoản</div>
        <div className="btn-feature">
          <CustomTooltip title="Tạo tài khoản" placement="bottomRight">
            <CustomButton
              text="+"
              onClick={() => {
                setIsOpenRegister(true);
                setIsEdit(false);
              }}
            />
          </CustomTooltip>
        </div>
      </div>
      <div className="page-search">
        <Search />
      </div>
      <div className="page-table">
        <CustomTable
          columns={columns}
          dataSource={data}
          locale={{
            emptyText: <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="Không có kết quả tìm kiếm." />,
          }}
        />
      </div>
      <div className="page-pagination">
        <Pagination total={100} current={5} pageSize={10} />
      </div>
      <ModalRegister
        isEdit={isEdit}
        isOpen={isOpenRegister}
        onClose={() => {
          setIsOpenRegister(false);
        }}
      />
      <ModalDelete isOpen={isOpenDelete} data={data[0]} onClose={() => setIsOpenDelete(false)} />
      <ModalHistory isOpen={isOpenHistory} onClose={() => setIsOpenHistory(false)} />
    </div>
  );
};

export default FaceRecognitionManagement;
