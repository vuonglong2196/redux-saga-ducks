import React, { useState } from 'react';
import { Empty } from 'antd';
import PropTypes from 'prop-types';
import CustomModal from '../../../components/common/CustomModal/CustomModal';
import CustomButton from '../../../components/common/CustomButton/CustomButton';
import CustomTable from '../../../components/common/CustomTable/CustomTable';
import CustomTooltip from '../../../components/common/CustomTooltip/CustomTooltip';
import CustomIcon from '../../../components/common/CustomIcon/CustomIcon';
import Pagination from '../../../components/common/CustomPagination/CustomPagination';
import iconLogout from '../../../assets/img/iconHeader/logout.svg';

import './style.scss';

const ModalHistory = (props) => {
  const { isOpen, onClose } = props;
  const [data] = useState([
    {
      ID: 'DF456',
      loginIP: '192.168.168.168',
      agent: 'Windows',
      loginTime: '23/09/2021 - 13:30:30',
      logoutTime: '23/09/2021 - 13:30:30',
    },
  ]);

  const columns = [
    {
      key: 'ID',
      title: 'ID',
      width: '50px',
      dataIndex: 'ID',
    },
    {
      key: 'loginIP',
      title: 'IP đăng nhập',
      sorter: true,
      width: '15%',
      showSorterTooltip: false,
      dataIndex: 'loginIP',
    },
    {
      key: 'agent',
      title: 'Hệ điều hành',
      sorter: true,
      width: 120,
      showSorterTooltip: false,
      dataIndex: 'agent',
    },
    {
      key: 'loginTime',
      title: 'Thời gian đăng nhập',
      sorter: true,
      width: 210,
      showSorterTooltip: false,
      dataIndex: 'loginTime',
    },
    {
      key: 'logoutTime',
      title: 'Thời gian đăng xuất',
      sorter: true,
      width: 210,
      showSorterTooltip: false,
      dataIndex: 'logoutTime',
    },
    {
      key: 'action',
      width: 80,
      align: 'center',
      title: 'Thao tác',
      render: () => (
        <div className="table-action">
          <CustomTooltip title="Đăng xuất ngay" placement="bottom">
            <div>
              <CustomIcon icon={iconLogout} />
            </div>
          </CustomTooltip>
        </div>
      ),
    },
  ];

  return (
    <CustomModal
      width={1000}
      title="Lịch sử đăng nhập"
      visible={isOpen}
      onCancel={onClose}
      footer={(
        <div className="action-modal">
          <CustomButton text="Thoát" onClick={onClose} />
        </div>
      )}
    >
      <div className="content-modal">
        <div className="page-table">
          <CustomTable
            columns={columns}
            dataSource={data}
            locale={{
              emptyText: <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="Không có kết quả tìm kiếm." />,
            }}
          />
        </div>
        <div className="page-pagination">
          <Pagination total={50} current={5} pageSize={10} />
        </div>
      </div>
    </CustomModal>
  );
};

ModalHistory.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ModalHistory;
