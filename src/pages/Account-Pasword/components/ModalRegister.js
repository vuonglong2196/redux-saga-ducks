import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'antd';
import CustomModal from '../../../components/common/CustomModal/CustomModal';
import CustomButton from '../../../components/common/CustomButton/CustomButton';
import CustomInput from '../../../components/common/CustomInput/CustomInput';
import Label from '../../../components/common/CustomLabel/CustomLabel';
import {
  checkUserName, checkFormatPassword, checkEmail, checkPhoneNumber,
} from '../../../utils/function';
import './style.scss';

const ModalRegister = (props) => {
  const { isOpen, onClose } = props;
  const [form] = Form.useForm();

  const onSubmitForm = () => {
    form.validateFields().then(() => {});
  };

  useEffect(() => () => form.resetFields(), [isOpen, form]);

  return (
    <CustomModal
      width={352}
      title="Tạo mới tài khoản"
      visible={isOpen}
      onCancel={onClose}
      footer={(
        <div className="action-modal">
          <CustomButton onClick={onSubmitForm} text="Tạo mới" />
        </div>
      )}
    >
      <div className="content-modal">
        <Form form={form} layout="vertical" autoComplete="off">
          <Label title="Tên đầy đủ" isRequire />
          <Form.Item label="" name="fullname" rules={[{ required: true, message: 'Tên đầy đủ không được để trống!' }]}>
            <CustomInput />
          </Form.Item>
          <Label title="Tên đăng nhập" isRequire />
          <Form.Item
            label=""
            name="username"
            rules={[
              { required: true, message: 'Tên đăng nhập không được để trống!' },
              () => ({
                validator(_, value) {
                  if (value && !checkUserName(value)) {
                    return Promise.reject(
                      new Error('Tên đăng nhập không đúng định dạng (chỉ nhập chữ không dấu và số)'),
                    );
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <CustomInput />
          </Form.Item>
          <Label title="Mật khẩu" isRequire />
          <Form.Item
            label=""
            name="password"
            rules={[
              { required: true, message: 'Mật khẩu không được để trống!' },
              () => ({
                validator(_, value) {
                  if (value && !checkFormatPassword(value)) {
                    return Promise.reject(
                      new Error(
                        'Nhập tối thiểu 8 ký tự bao gồm: Chữ, số, và ký tự viết HOA, viết thường, ký tự đặc biệt!',
                      ),
                    );
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Label title="Nhập lại mật khẩu" isRequire />
          <Form.Item
            label=""
            name="confirmPass"
            dependencies={['password']}
            rules={[
              { required: true, message: 'Nhập lại mật khẩu không được để trống!' },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (value && getFieldValue('password') !== value) {
                    return Promise.reject(new Error('Nhập lại mật khẩu không trùng khớp với Mật khẩu!'));
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Label title="Email" isRequire />
          <Form.Item
            label=""
            name="email"
            rules={[
              { required: true, message: 'Email không được để trống!' },
              () => ({
                validator(_, value) {
                  if (value && !checkEmail(value)) {
                    return Promise.reject(new Error('Email không đúng định dạng!'));
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <CustomInput />
          </Form.Item>
          <Label title="Số điện thoại" isRequire />
          <Form.Item
            label=""
            name="phoneNumber"
            rules={[
              { required: true, message: 'Số điện thoại không được để trống!' },
              () => ({
                validator(_, value) {
                  if (value && !checkPhoneNumber(value)) {
                    return Promise.reject(new Error('Số điện thoại sai định dạng!'));
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <CustomInput />
          </Form.Item>
        </Form>
      </div>
    </CustomModal>
  );
};

ModalRegister.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ModalRegister;
