import React from 'react';
import PropTypes from 'prop-types';
import CustomModal from '../../../components/common/CustomModal/CustomModal';
import CustomButton from '../../../components/common/CustomButton/CustomButton';

import './style.scss';

const ModalDelete = (props) => {
  const { isOpen, onClose, data } = props;

  return (
    <CustomModal
      width={352}
      title="Xác nhận xoá tài khoản"
      visible={isOpen}
      onCancel={onClose}
      footer={(
        <div className="action-modal">
          <CustomButton className="cancel" onClick={onClose} text="Huỷ" />
          <CustomButton text="Xoá" />
        </div>
      )}
    >
      <div className="content-modal">
        <div className="content-notify">
          {`Bạn có chắc chắn muốn xoá tài khoản 
          ${data.username} 
          không?`}
        </div>
      </div>
    </CustomModal>
  );
};

ModalDelete.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
};

export default ModalDelete;
