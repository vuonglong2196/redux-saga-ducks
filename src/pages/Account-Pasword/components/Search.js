import React from 'react';
import { Col, Form } from 'antd';
import CustomButton from '../../../components/common/CustomButton/CustomButton';
import CustomInput from '../../../components/common/CustomInput/CustomInput';

import './style.scss';

const Search = () => (
  <div className="div-search-account">
    <Form labelCol={{ span: 8 }} wrapperCol={{ span: 16 }} initialValues={{ remember: true }} autoComplete="off">
      <Col span={10}>
        <Form.Item label="Tên đăng nhập" name="username">
          <CustomInput placeholder="Tên đăng nhập" />
        </Form.Item>
      </Col>
      <Col span={10}>
        <Form.Item label="Email" name="email">
          <CustomInput placeholder="Email" />
        </Form.Item>
      </Col>
      <Col span={10}>
        <Form.Item label="Số điện thoại" name="phoneNumber">
          <CustomInput placeholder="Số điện thoại" />
        </Form.Item>
      </Col>
      <Col span={4}>
        <Form.Item>
          <CustomButton text="Tìm kiếm" />
        </Form.Item>
      </Col>
    </Form>
  </div>
);

export default Search;
