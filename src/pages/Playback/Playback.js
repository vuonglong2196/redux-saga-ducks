import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import headerActions from '../../redux/ducks/header/actions';

const Playback = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(headerActions.isShowFullScreen(true));

    return () => {
      dispatch(headerActions.isShowFullScreen(false));
    };
  }, [dispatch]);

  return <div>Playback pages</div>;
};

export default Playback;
