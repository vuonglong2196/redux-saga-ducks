import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import headerActions from '../../redux/ducks/header/actions';

const VideoWall = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(headerActions.isShowMic(true));
    dispatch(headerActions.isShowFullScreen(true));

    return () => {
      dispatch(headerActions.isShowMic(false));
      dispatch(headerActions.isShowFullScreen(false));
    };
  }, [dispatch]);

  return <div>VideoWall pages</div>;
};

export default VideoWall;
