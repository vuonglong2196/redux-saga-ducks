import { SET_LIST } from './types';

const initialState = {
  mapsData: {
    mapsList: {},
  },
};

const mapsReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LIST: {
      return {
        ...state,
        mapsData: {
          ...state.mapsData,
          mapsList: action.payload,
        },
      };
    }
    default:
      return state;
  }
};

export default mapsReducer;
