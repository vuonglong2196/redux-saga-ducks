export const GET_LIST = 'maps/GET_LIST';
export const SET_LIST = 'maps/SET_LIST';

const mapsTypes = {
  GET_LIST,
  SET_LIST,
};

export default mapsTypes;
