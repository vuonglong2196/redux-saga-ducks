import { GET_LIST, SET_LIST } from './types';

const getListMaps = () => ({
  type: GET_LIST,
});

const setListMaps = (data) => ({
  type: SET_LIST,
  payload: { data },
});

const mapsActions = {
  getListMaps,
  setListMaps,
};

export default mapsActions;
