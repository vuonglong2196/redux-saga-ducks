import mapsReducer from './reducer';

export { default as mapsActions } from './actions';
export { default as mapsTypes } from './types';

export default mapsReducer;
