import { combineReducers } from 'redux';
import mapsReducer from './maps';
import layoutReducer from './layout';
import headerReducer from './header';

export default combineReducers({
  mapsReducer,
  layoutReducer,
  headerReducer,
});
