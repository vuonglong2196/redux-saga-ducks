// Sidebar
export const IS_SHOW_MIC = 'header/IS_SHOW_MIC';
export const IS_SHOW_FULL_SCREEN = 'header/IS_SHOW_FULL_SCREEN';
export const IS_FULL_SCREEN = 'header/IS_FULL_SCREEN';
export const EXIT_FULL_SCREEN = 'header/EXIT_FULL_SCREEN';

const headerTypes = {
  IS_SHOW_MIC,
  IS_SHOW_FULL_SCREEN,
  IS_FULL_SCREEN,
  EXIT_FULL_SCREEN,
};

export default headerTypes;
