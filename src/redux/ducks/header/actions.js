import {
  IS_SHOW_MIC, IS_SHOW_FULL_SCREEN, IS_FULL_SCREEN, EXIT_FULL_SCREEN,
} from './types';

const isShowMic = (isShow) => ({
  type: IS_SHOW_MIC,
  payload: isShow,
});

const isShowFullScreen = (isShow) => ({
  type: IS_SHOW_FULL_SCREEN,
  payload: isShow,
});

const isFullScreen = () => ({
  type: IS_FULL_SCREEN,
});

const exitFullScreen = () => ({
  type: EXIT_FULL_SCREEN,
});

const headerActions = {
  isShowMic,
  isShowFullScreen,
  isFullScreen,
  exitFullScreen,
};

export default headerActions;
