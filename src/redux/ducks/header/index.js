import headerReducer from './reducer';

export { default as headerActions } from './actions';
export { default as headerTypes } from './types';

export default headerReducer;
