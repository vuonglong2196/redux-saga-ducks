import {
  IS_SHOW_MIC, IS_SHOW_FULL_SCREEN, IS_FULL_SCREEN, EXIT_FULL_SCREEN,
} from './types';

export const initialState = {
  headerData: {
    isShowMic: false,
  },
  screen: {
    isFullScreen: false,
    exitFullScreen: false,
    isShowFullScreen: false,
  },
};

const headerReducer = (state = initialState, action) => {
  switch (action.type) {
    case IS_SHOW_MIC: {
      return {
        ...state,
        headerData: {
          ...state.headerData,
          isShowMic: action.payload,
        },
      };
    }

    case IS_SHOW_FULL_SCREEN: {
      return {
        ...state,
        screen: {
          ...state.screen,
          isShowFullScreen: action.payload,
        },
      };
    }

    case IS_FULL_SCREEN: {
      return {
        ...state,
        screen: {
          ...state.screen,
          isFullScreen: true,
          exitFullScreen: false,
        },
      };
    }

    case EXIT_FULL_SCREEN: {
      return {
        ...state,
        screen: {
          ...state.screen,
          isFullScreen: false,
          exitFullScreen: true,
        },
      };
    }
    default:
      return state;
  }
};

export default headerReducer;
