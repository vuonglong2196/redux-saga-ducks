import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectHeaderReducer = (state) => state.headerReducer || initialState;

const selectIsShowMic = () => 
  createSelector(
    selectHeaderReducer, 
    (state) => state.headerData.isShowMic
  );

const selectIsShowFullScreen = () => 
  createSelector(
    selectHeaderReducer, 
    (state) => state.screen.isShowFullScreen
  );

const selectIsFullScreen = () => 
  createSelector(
    selectHeaderReducer, 
    (state) => state.screen.isFullScreen
  );

const selectExitFullScreen = () => 
  createSelector(
    selectHeaderReducer, 
    (state) => state.screen.exitFullScreen
  );

export { selectIsShowMic, selectIsShowFullScreen, selectIsFullScreen, selectExitFullScreen };
