import { IS_OPEN_SIDEBAR } from './types';

const initialState = {
  sidebarData: {
    isOpen: false,
  },
};

const layoutReducer = (state = initialState, action) => {
  switch (action.type) {
    case IS_OPEN_SIDEBAR: {
      return {
        ...state,
        sidebarData: {
          ...state.sidebarData,
          isOpen: action.payload,
        },
      };
    }
    default:
      return state;
  }
};

export default layoutReducer;
