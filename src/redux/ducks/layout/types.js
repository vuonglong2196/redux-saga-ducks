// Sidebar
export const IS_OPEN_SIDEBAR = 'layout/IS_OPEN_SIDEBAR';
export const SHOW_HIDE_SIDEBAR = 'layout/SHOW_HIDE_SIDEBAR';

const layoutTypes = {
  IS_OPEN_SIDEBAR,
  SHOW_HIDE_SIDEBAR,
};

export default layoutTypes;
