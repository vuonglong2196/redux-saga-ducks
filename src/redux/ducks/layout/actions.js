import { IS_OPEN_SIDEBAR, SHOW_HIDE_SIDEBAR } from './types';

const isOpenSidebar = (boo) => ({
  type: IS_OPEN_SIDEBAR,
  payload: boo,
});

const showHideSidebar = (boo) => ({
  type: SHOW_HIDE_SIDEBAR,
  payload: boo,
});

const layoutActions = {
  showHideSidebar,
  isOpenSidebar,
};

export default layoutActions;
