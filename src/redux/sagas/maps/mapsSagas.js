import { call, put, takeLatest } from 'redux-saga/effects';
import { axiosGet } from '../../../api/requests';
import mapsActions from '../../ducks/maps/actions';
import { GET_LIST } from '../../ducks/maps/types';

export function* getListMaps() {
  yield put(actions.requestBegin());
  const path = `/regionservice/getAllRegions`;

  try {
    const res = yield call(axiosGet, path);
    yield put(mapsActions.setListMaps(res));
  } catch (err) {
    yield put(actions.requestFail(err));
  }
}

export default function* watchRequestCameras() {
  yield takeLatest(GET_LIST, getListMaps);
}
