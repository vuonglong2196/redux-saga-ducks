import { all, call } from 'redux-saga/effects';
import mapsSaga from './maps/mapsSagas';

export default function* rootSaga() {
  try {
    yield all([call(mapsSaga)]);
  } catch (err) {
    // code
  }
}
