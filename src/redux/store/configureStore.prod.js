import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from '../ducks/index';
import SagaManager from '../sagas/SagaManager';

export default function configureStore(initialState) {
  const sagaMiddleware = createSagaMiddleware();
  const enhancer = applyMiddleware(sagaMiddleware);
  const store = createStore(rootReducer, initialState, enhancer);

  SagaManager.startSagas(sagaMiddleware);
  return store;
}
