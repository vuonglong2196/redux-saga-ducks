import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from '../ducks/index';
import SagaManager from '../sagas/SagaManager';

const duck = require('../ducks');

const saga = require('../sagas/SagaManager');

export default function configureStore(initialState) {
  const sagaMiddleware = createSagaMiddleware();
  // Connect developer tools (tool that maps to use the Developer
  // Tools Redux tab when installing Chrome plugins)
  const composeEnhancers = composeWithDevTools({});
  // Injection of settings to include sagas when setting up the store
  const enhancer = composeEnhancers(applyMiddleware(sagaMiddleware));

  // Store settings (Redux settings)
  const store = createStore(rootReducer, initialState, enhancer);

  // Middleware (Redux-Saga) operation
  SagaManager.startSagas(sagaMiddleware);

  if (module.hot) {
    module.hot.accept('../ducks', () => store.replaceReducer(duck.default));
    module.hot.accept('../sagas/SagaManager', () => {
      SagaManager.cancelSagas(store);
      saga.default.startSagas(sagaMiddleware);
    });
  }

  return store;
}
