const configureStoreProd = require('./configureStore.prod');
const configureStoreDev = require('./configureStore.dev');

let configureStore = () => {};
if (process.env.NODE_ENV === 'production') {
  configureStore = configureStoreProd.default;
} else {
  configureStore = configureStoreDev.default;
}

const initialState = undefined;
const store = configureStore(initialState);
export default store;
