import locale from 'antd/lib/locale/vi_VN';
import i18next from 'i18next';

export const localePr = {
  ...locale,
  DatePicker: {
    ...locale.DatePicker,
    lang: {
      placeholder: i18next.t('locale'),
    },
  },
};
