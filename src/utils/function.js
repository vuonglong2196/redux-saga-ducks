import i18n from 'i18next';

export const changeLanguage = (lang) => {
  i18n.changeLanguage(lang);
  localStorage.setItem('i18language', lang);
};

export const checkFormatPassword = (pass) => {
  const regexTV = new RegExp('^[a-zA-Zd!@#$%&*]');
  const regexPass = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})');

  if (regexTV.test(pass)) {
    return regexPass.test(pass);
  }
  return false;
};

export const checkSpecialCharacters = (char) => {
  const regexSpecialCharacter = new RegExp('[!@#$%^&*()+\\=\\[\\]{};:"\'\\|,.<>\\/?]');
  return regexSpecialCharacter.test(char);
};

export const checkUserName = (char) => {
  const regex = new RegExp('^[a-zA-Z0-9+]*$');
  return regex.test(char);
};

export const checkEmail = (char) => {
  const regex = new RegExp(
    '^(([^<>()[\\]\\.,;:\\s@\\"]+(\\.[^<>()[\\]\\.,;:\\s@\\"]+)*)|(\\".+\\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$',
  );
  return regex.test(char);
};

export const checkPhoneNumber = (char) => {
  const regex = new RegExp('(03|05|07|08|09|01|\\+84[2|6|8|9])+([0-9]{8})');
  return regex.test(char);
};
