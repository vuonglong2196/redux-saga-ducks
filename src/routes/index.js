import AccountPasword from '../pages/Account-Pasword';
import AlarmManagement from '../pages/AlarmManagement/AlarmManagement';
import Page404 from '../pages/auth/Page404/Page404';
import CameraList from '../pages/CameraList/CameraList';
import EventManagement from '../pages/EventManagement/EventManagement';
import FaceRecognitionManagement from '../pages/FaceRecognitionManagement';
import Maps from '../pages/maps/Maps';
import Playback from '../pages/Playback/Playback';
import SearchEvents from '../pages/SearchEvents/SearchEvents';
import Trace from '../pages/Trace';
import VideoWall from '../pages/VideoWall/VideoWall';
import ObjectRecognition from '../pages/ObjectRecognition/ObjectRecognition';
import FollowUpAlerts from '../pages/FollowUpAlerts/FollowUpAlerts';
import links from '../components/constant/Routes';
import AuthForm from '../pages/auth/login/AuthForm/AuthForm';

const pageRoutes = {
  pathParent: links.PAGE,
  name: 'Pages',
  children: [
    {
      no: 1,
      path: [links.CCTV, links.HOME],
      name: 'Video wall',
      component: VideoWall,
    },
    {
      no: 2,
      path: links.PLAY_BACK,
      name: 'Play back',
      component: Playback,
    },
    {
      no: 3,
      path: links.TRACE,
      name: 'Trace',
      component: Trace,
    },
    {
      no: 4,
      path: links.EVENT,
      name: 'Search event',
      component: SearchEvents,
    },
    {
      no: 5,
      path: links.FACE_MANAGEMENT,
      name: 'Face Recognition Management',
      component: FaceRecognitionManagement,
    },
    {
      no: 6,
      path: links.MAPS,
      name: 'Maps',
      component: Maps,
    },
    {
      no: 7,
      path: links.CAMERA,
      name: 'Camera List',
      component: CameraList,
    },
    {
      no: 8,
      path: links.USER,
      name: 'Account/Password',
      component: AccountPasword,
    },
    {
      no: 9,
      path: links.EVENT_MANAGEMENT,
      name: 'Event manager',
      component: EventManagement,
    },
    {
      no: 10,
      path: links.ALARM,
      name: 'Alarm Management',
      component: AlarmManagement,
    },
    {
      no: 11,
      path: links.DETECT,
      name: 'ObjectRecognition',
      component: ObjectRecognition,
    },
    {
      no: 12,
      path: links.ALARM_V2,
      name: 'Follow UpAlerts',
      component: FollowUpAlerts,
    },
  ],
};

const authRoutes = {
  pathParent: links.AUTH,
  name: 'Auth',
  children: [
    {
      path: links.PAGE404,
      name: '404 Page',
      component: Page404,
    },
    {
      path: links.LOGIN,
      name: 'Sign In',
      component: AuthForm,
    },
    {
      path: links.FORGOT_PASSWORD,
      name: 'Forgot Password',
      component: AuthForm,
    },
    {
      path: links.CHANGE_PASSWORD,
      name: 'Change Password',
      component: AuthForm,
    },
    {
      path: links.CONFIRM_EMAIL,
      name: 'Forgot Password',
      component: AuthForm,
    },
    {
      path: links.CONFIRM_OTP,
      name: 'Confirm OTP',
      component: AuthForm,
    },
  ],
};

// Dashboard specific routes
export const dashboard = [pageRoutes];

// Auth specific routes
export const page = [authRoutes];

// All routes
export default [pageRoutes, authRoutes];
