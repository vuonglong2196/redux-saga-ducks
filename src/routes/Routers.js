import React from 'react';
// import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import { dashboard as dashboardRoutes, page as pageRoutes } from './index';
import DashboardLayout from '../layouts/Dashboard';
import AuthLayout from '../layouts/Auth';
import Page404 from '../pages/auth/Page404/Page404';

const childRoutes = (Layout, routes) => routes.map(({ children, pathParent, componentParent: ComponentParent }) => (children ? (
// Route item with children
  children.map(({ path, component: Component }) => (
    <Route
      key={path}
      path={path}
      exact
      render={(props) => (
        // <Layout>
        <Component {...props} />
        // </Layout>
      )}
    />
  ))
) : (
// Route item without children
  <Route
    key={pathParent}
    path={pathParent}
    exact
    render={(props) => (
      <Layout>
        <ComponentParent {...props} />
      </Layout>
    )}
  />
)));

const Routes = () => (
  <DashboardLayout>
    <Switch>
      {childRoutes(DashboardLayout, dashboardRoutes)}
      {childRoutes(AuthLayout, pageRoutes)}
      <Route
        render={() => (
          <AuthLayout>
            <Page404 />
          </AuthLayout>
        )}
      />
    </Switch>
  </DashboardLayout>
);

Routes.propTypes = {
  // children: PropTypes.node.isRequired,
  // location: PropTypes.object,
};

export default Routes;
