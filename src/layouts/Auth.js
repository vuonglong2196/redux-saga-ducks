import React from 'react';
import PropTypes from 'prop-types';

const Auth = ({ children }) => (
  <>
    <div className="d-table-cell align-middle">{children}</div>
  </>
);

Auth.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Auth;
