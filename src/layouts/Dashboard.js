import React from 'react';
import { Layout } from 'antd';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import '../translate/i18n';
import Headers from '../components/header/Headers';
import Sidebar from '../components/sidebar/Sidebar';
import './Layout.scss';
import links from '../components/constant/Routes';

const Dashboard = ({ children, location }) => {
  const { Content } = Layout;

  const exceptionLinks = [
    links.LOGIN,
    links.FORGOT_PASSWORD,
    links.CHANGE_PASSWORD,
    links.CONFIRM_EMAIL,
    links.CONFIRM_OTP,
  ];

  return (
    <Layout>
      {!exceptionLinks.includes(location.pathname ? location.pathname : links.HOME) ? <Sidebar /> : ''}
      <Layout className="site-layout">
        {!exceptionLinks.includes(location.pathname ? location.pathname : links.HOME) ? <Headers /> : ''}
        <Content
          className={
            !exceptionLinks.includes(location.pathname ? location.pathname : links.HOME)
              ? 'site-layout-background'
              : 'auth-wrapper'
          }
        >
          {children}
        </Content>
      </Layout>
    </Layout>
  );
};

Dashboard.defaultProps = {
  location: {},
};

Dashboard.propTypes = {
  children: PropTypes.node.isRequired,
  location: PropTypes.object,
};

export default withTranslation()(withRouter(Dashboard));
