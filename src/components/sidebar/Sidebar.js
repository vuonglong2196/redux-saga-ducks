import React from 'react';
import { Menu, Layout } from 'antd';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import logo from '../../assets/img/iconSidebar/logo2021.png';
import { siderLink } from './index';
import './Sidebar.scss';

const Sidebar = () => {
  const { SubMenu } = Menu;
  const { Sider } = Layout;

  const isOpen = useSelector((state) => state.layoutReducer.sidebarData.isOpen);
  return (
    <Sider
      trigger={null}
      collapsible
      collapsed={isOpen}
      width={242}
      className="side-bar-aside"
    >
      <div className="logo"><img src={logo} width="25" height="25" alt="" /></div>
      <Menu
        theme="dark"
        mode="inline"
        defaultOpenKeys={['6', '7']}
        defaultSelectedKeys={window.location.pathname}
      >
        {siderLink.pageLinks.children.map((item) => {
          if (item.children && item.children.length > 0) {
            return (
              <SubMenu key={item.no} icon={item.icon} title={item.name}>
                <Menu.ItemGroup title={item.name}>
                  {item.children.map((childrens) => (
                    <Menu.Item key={childrens.to}>
                      <Link to={childrens.to}>
                        {childrens.name}
                      </Link>
                    </Menu.Item>
                  ))}
                </Menu.ItemGroup>
              </SubMenu>
            );
          }
          return (
            <Menu.Item key={item.to} icon={item.icon}>
              <Link to={item.to}>
                {item.name}
              </Link>
            </Menu.Item>
          );
        })}
      </Menu>
    </Sider>
  );
};

export default Sidebar;
