import React from 'react';
import live from '../../assets/img/iconSidebar/live.png';
import replay from '../../assets/img/iconSidebar/replay.png';
import map from '../../assets/img/iconSidebar/map.png';
import camera from '../../assets/img/iconSidebar/camera.png';
import manage from '../../assets/img/iconSidebar/manage.png';
import faceid from '../../assets/img/iconSidebar/faceid.png';
import warning from '../../assets/img/iconSidebar/warning.png';
import user from '../../assets/img/iconSidebar/user.png';
import links from '../constant/Routes';

const pageLinks = {
  children: [
    {
      no: 1,
      icon: <img src={live} alt="" />,
      to: links.CCTV,
      name: 'Giám sát',
      children: [],
    },
    {
      no: 2,
      icon: <img src={replay} alt="" />,
      to: links.PLAY_BACK,
      name: 'Xem lại',
      children: [],
    },
    {
      no: 3,
      icon: <img src={map} alt="" />,
      to: links.MAPS,
      name: 'Camera trên bản đồ',
      children: [],
    },
    {
      no: 4,
      icon: <img src={camera} alt="" />,
      to: links.CAMERA,
      name: 'Danh sách camera',
      children: [],
    },
    {
      no: 5,
      icon: <img src={manage} alt="" />,
      to: links.DETECT,
      name: 'Tìm kiếm tập trung',
      children: [],
    },
    {
      no: 6,
      icon: <img src={faceid} alt="" />,
      to: '',
      name: 'Nhận diện khuôn mặt',
      children: [
        {
          no: '6n1',
          icon: '',
          to: links.FACE_MANAGEMENT,
          name: 'Quản lý nhận diện khuôn mặt',
        },
        {
          no: '6n2',
          icon: '',
          to: links.TRACE,
          name: 'Truy vết',
        },
      ],
    },
    {
      no: 7,
      icon: <img src={warning} alt="" />,
      to: '',
      name: 'Cảnh báo và sự kiện',
      children: [
        {
          no: '7n1',
          icon: '',
          to: links.EVENT_MANAGEMENT,
          name: 'Sự kiện',
        },
        {
          no: '7n2',
          icon: '',
          to: links.ALARM,
          name: 'Cảnh báo',
        },
      ],
    },
    {
      no: 8,
      icon: <img src={user} alt="" />,
      to: links.USER,
      name: 'Tài khoản/Mật khẩu',
    },
  ],
};

export const siderLink = {
  pageLinks,
};

export default {
  pageLinks,
};
