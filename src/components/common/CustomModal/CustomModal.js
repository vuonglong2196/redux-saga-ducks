import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'antd';
import './CustomModal.scss';

const CustomModal = (props) => {
  const {
    visible, title, children, onCancel, footer, width,
  } = props;
  return (
    <Modal width={width} footer={footer} title={title} visible={visible} onCancel={onCancel}>
      {children}
    </Modal>
  );
};

CustomModal.defaultProps = {
  footer: null,
  width: 352,
};

CustomModal.propTypes = {
  children: PropTypes.node.isRequired,
  visible: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  onCancel: PropTypes.func.isRequired,
  footer: PropTypes.node,
  width: PropTypes.number,
};

export default CustomModal;
