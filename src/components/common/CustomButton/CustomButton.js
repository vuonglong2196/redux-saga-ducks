import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';
import './CustomButton.scss';

const CustomButton = ({
  className, text, typeButton, ...props
}) => (
  <Button
    className={`custom-button ${typeButton && typeButton === 'gradient' ? 'gradient' : ''}${className || ''}`}
    {...props}
  >
    {text || ''}
  </Button>
);

CustomButton.defaultProps = {
  className: '',
  typeButton: '',
};

CustomButton.propTypes = {
  className: PropTypes.string,
  text: PropTypes.any.isRequired,
  typeButton: PropTypes.string,
};

export default CustomButton;
