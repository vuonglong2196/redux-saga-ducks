import React from 'react';
import { Input } from 'antd';
import './CustomTextArea.scss';

const CustomTextArea = (props) => <Input.TextArea {...props} />;

CustomTextArea.propTypes = {};

export default CustomTextArea;
