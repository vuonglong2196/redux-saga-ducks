import React from 'react';
import { Select } from 'antd';
import PropTypes, { object } from 'prop-types';
import CustomIcon from '../CustomIcon/CustomIcon';
import dropdown from '../../../assets/img/iconHeader/dropdown.svg';
import './CustomSelect.scss';

const CustomSelect = (props) => {
  const { lstOption } = props;
  return (
    <Select {...props} suffixIcon={<CustomIcon icon={dropdown} />}>
      {lstOption.map((option) => (
        <Select.Option key={option.value} value={option.value}>
          {option.title}
        </Select.Option>
      ))}
    </Select>
  );
};

CustomSelect.propTypes = {
  lstOption: PropTypes.arrayOf(object).isRequired,
};

export default CustomSelect;
