import React from 'react';
import { TreeSelect } from 'antd';
import PropTypes, { object } from 'prop-types';
import CustomIcon from '../CustomIcon/CustomIcon';
import dropdown from '../../../assets/img/iconHeader/dropdown.svg';
import './CustomTreeSelect.scss';

const CustomTreeSelect = (props) => {
  const { treeData, treeCheckable } = props;
  return (
    <TreeSelect
      treeData={treeData}
      treeCheckable={treeCheckable}
      allowClear
      showSearch
      treeDefaultExpandAll
      showArrow
      {...props}
      suffixIcon={<CustomIcon icon={dropdown} />}
    />
  );
};

CustomTreeSelect.defaultProps = {
  treeCheckable: true,
};

CustomTreeSelect.propTypes = {
  treeData: PropTypes.arrayOf(object).isRequired,
  treeCheckable: PropTypes.bool,
};

export default CustomTreeSelect;
