import React from 'react';
import { Radio } from 'antd';
import PropTypes from 'prop-types';
import './CustomRadio.scss';

const CustomRadio = ({ value, title }) => <Radio value={value}>{title}</Radio>;

CustomRadio.defaultProps = {
  value: 0,
};

CustomRadio.propTypes = {
  value: PropTypes.number.isRequired || PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

export default CustomRadio;
