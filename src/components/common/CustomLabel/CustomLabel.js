import React from 'react';
import PropTypes from 'prop-types';
import './CustomLabel.scss';

const CustomLabel = (props) => {
  const { title, isRequire } = props;

  return (
    <div className="label">
      <span className="title">{title}</span>
      {isRequire && <span className="require">*</span>}
    </div>
  );
};

CustomLabel.defaultProps = {
  isRequire: false,
};

CustomLabel.propTypes = {
  title: PropTypes.string.isRequired,
  isRequire: PropTypes.bool,
};

export default CustomLabel;
