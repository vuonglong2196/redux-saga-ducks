import React from 'react';
import PropTypes from 'prop-types';
import { Tooltip } from 'antd';
import './CustomTooltip.scss';

const CustomTooltip = (props) => {
  const {
    children, title, color, placement,
  } = props;
  return (
    <Tooltip title={title} color={color} placement={placement} {...props}>
      {children}
    </Tooltip>
  );
};

CustomTooltip.defaultProps = {
  title: '',
  color: '#FFFFFF',
  placement: 'top',
};

CustomTooltip.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string,
  color: PropTypes.string,
  placement: PropTypes.string,
};

export default CustomTooltip;
