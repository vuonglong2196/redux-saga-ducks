import React from 'react';
import PropTypes from 'prop-types';
import SVG from 'react-inlinesvg';

import './CustomIcon.scss';

const CustomIcon = ({ icon }, props) => <SVG src={icon} {...props} />;

CustomIcon.propTypes = {
  icon: PropTypes.any.isRequired,
};

export default CustomIcon;
