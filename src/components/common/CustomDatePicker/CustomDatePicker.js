import React from 'react';
import { DatePicker } from 'antd';
import './CustomDatePicker.scss';

const CustomDatePicker = (props) => <DatePicker {...props} />;

CustomDatePicker.propTypes = {};

export default CustomDatePicker;
