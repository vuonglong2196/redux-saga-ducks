import React from 'react';
import PropTypes from 'prop-types';
import { Upload } from 'antd';
import './UploadFile.scss';
import CustomInput from '../CustomInput/CustomInput';
import CustomIcon from '../CustomIcon/CustomIcon';
import iconCamera from '../../../assets/img/iconCommon/camera.svg';

const url = 'https://www.mocky.io/v2/5cc8019d300000980a055e76';

function UploadFile(props) {
  const { children, multiple, accept, disabled, fileList, onChange, fileName } = props;

  return (
    <Upload
      multiple={multiple}
      accept={accept}
      disabled={disabled}
      action={url}
      fileList={fileList}
      onChange={onChange}
    >
      <CustomInput readOnly value={fileName} />
      <CustomIcon icon={iconCamera} />
    </Upload>
  );
}

UploadFile.defaultProps = {
  multiple: false,
  disabled: false,
  accept: '.jpg, .png',
  fileList: [],
  fileName: '',
};

UploadFile.propTypes = {
  children: PropTypes.node.isRequired,
  multiple: PropTypes.bool,
  disabled: PropTypes.bool,
  accept: PropTypes.string,
  fileList: PropTypes.array,
  onChange: PropTypes.func.isRequired,
  fileName: PropTypes.string,
};

export default UploadFile;
