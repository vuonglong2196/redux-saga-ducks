import React from 'react';
import { Input } from 'antd';
import './CustomInput.scss';

const CustomInput = (props) => <Input {...props} />;

CustomInput.propTypes = {};

export default CustomInput;
