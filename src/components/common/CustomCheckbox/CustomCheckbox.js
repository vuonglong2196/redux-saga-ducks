import React from 'react';
import PropTypes from 'prop-types';
import { Checkbox } from 'antd';
import './CustomCheckbox.scss';

const CustomCheckbox = ({ label, ...props }) => (
  <Checkbox {...props} className="custom-checkbox">
    {label || ''}
  </Checkbox>
);

CustomCheckbox.defaultProps = {
  label: '',
};

CustomCheckbox.propTypes = {
  label: PropTypes.string,
};

export default CustomCheckbox;
