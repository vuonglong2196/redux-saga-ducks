import React from 'react';
import PropTypes from 'prop-types';
import { Col, Pagination, Row } from 'antd';
import CustomSelect from '../CustomSelect/CustomSelect';
import CustomInput from '../CustomInput/CustomInput';
import CustomIcon from '../CustomIcon/CustomIcon';
import { pageSizeOptions } from '../../constant/constants';
import iconNext from '../../../assets/img/iconPagination/next.svg';
import iconPre from '../../../assets/img/iconPagination/previous.svg';

import './CustomPagination.scss';

const CustomPagination = (props) => {
  const { total, current, pageSize, onChangePage, onChangePageSize } = props;
  const itemRender = (curr, type, originalElement) => {
    if (type === 'prev') {
      return <CustomIcon icon={iconPre} />;
    }
    if (type === 'next') {
      return <CustomIcon icon={iconNext} />;
    }
    return originalElement;
  };

  return (
    <Row gutter={20} className="pagination">
      <Col span={11} className="d-flex-alg-center">
        <span>{`Hiển thị ${pageSize * (current - 1) + 1} - ${pageSize * current} trong ${total} bản ghi`}</span>
        <div>
          <CustomSelect value={pageSize} lstOption={pageSizeOptions} onChange={onChangePageSize} />
        </div>
      </Col>
      <Col span={13} className="d-flex-alg-center">
        <div>
          <Pagination
            showSizeChanger={false}
            total={total}
            current={current}
            pageSize={pageSize}
            itemRender={itemRender}
            showTitle={false}
            onChange={onChangePage}
          />
        </div>
        <div className="d-flex-alg-center">
          <span>Đi đến trang</span>
          <div>
            <CustomInput style={{ width: '50px' }} onChange={onChangePage} />
          </div>
        </div>
      </Col>
    </Row>
  );
};

CustomPagination.defaultProps = {
  total: 0,
  current: 1,
  pageSize: 10,
};

CustomPagination.propTypes = {
  total: PropTypes.number,
  current: PropTypes.number,
  pageSize: PropTypes.number,
  onChangePage: PropTypes.func.isRequired,
  onChangePageSize: PropTypes.func.isRequired,
};

export default CustomPagination;
