import React from 'react';
import { Table } from 'antd';
import './CustomTable.scss';

const CustomTable = (props) => <Table {...props} pagination={false} />;

CustomTable.propTypes = {};

export default CustomTable;
