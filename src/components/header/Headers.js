import React, { useEffect, useState } from 'react';
import {
  Button, Dropdown, Layout, Space, Menu,
} from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
// Components
import CustomIcon from '../common/CustomIcon/CustomIcon';
import CustomButton from '../common/CustomButton/CustomButton';
import ModalIntroduce from './components/ModalIntroduce';
import ModalChangePassword from './components/ModalChangePassword';
import ModalSettings from './components/ModalSettings';
import ModalLogout from './components/ModalLogout';
// Actions
import layoutActions from '../../redux/ducks/layout/actions';
import headerActions from '../../redux/ducks/header/actions';
// Selectors
import {
  selectIsShowMic, 
  selectIsShowFullScreen, 
  selectIsFullScreen
  } from '../../redux/ducks/header/selectors';
// Utils
import { changeLanguage } from '../../utils/function';
// Icon
import closeSideBar from '../../assets/img/iconSidebar/Vector.png';
import openSideBar from '../../assets/img/iconSidebar/Vectorr.png';
import infoUser from '../../assets/img/iconHeader/icon-user.svg';
import dropdown from '../../assets/img/iconHeader/dropdown.svg';
import mic from '../../assets/img/iconHeader/mic.svg';
import bell from '../../assets/img/iconHeader/bell.svg';
import full from '../../assets/img/iconHeader/full.svg';
import noFull from '../../assets/img/iconHeader/no-full.svg';
import introduce from '../../assets/img/iconHeader/introduce.svg';
import vietnamese from '../../assets/img/iconHeader/vietnamese.svg';
import english from '../../assets/img/iconHeader/english.svg';
import changePassword from '../../assets/img/iconHeader/change-password.svg';
import settings from '../../assets/img/iconHeader/settings.svg';
import logout from '../../assets/img/iconHeader/logout.svg';
import Rectangle from '../../assets/img/iconHeader/Rectangle.png';
// CSS
import './Headers.scss';

const { Header } = Layout;
const Headers = () => {
  const language = localStorage.getItem('i18language');
  const { t } = useTranslation();
  const isOpen = useSelector((state) => state.layoutReducer.sidebarData.isOpen);
  const isShowMic = useSelector(selectIsShowMic());
  const isShowFullScreen = useSelector(selectIsShowFullScreen());
  const isFullScreen = useSelector(selectIsFullScreen());
  const dispatch = useDispatch();
  const [time, setTime] = useState(moment().format('HH:mm:ss'));
  const [isOpenIntroduce, setIsOpenIntroduce] = useState(false);
  const [isOpenChangePassword, setIsOpenChangePassword] = useState(false);
  const [isOpenSettings, setIsOpenSettings] = useState(false);
  const [isOpenLogout, setIsOpenLogout] = useState(false);
  const checkPermissionsMic = () => {
    const permissions = navigator.mediaDevices.getUserMedia({ audio: true, video: false });
    permissions.then(() => {}).catch(() => {});
  };
  useEffect(() => {
    setInterval(() => {
      setTime(moment().format('HH:mm:ss'));
    }, 1000);
  }, []);
  const menu = (
    <Menu>
      <Menu.Item key="1" onClick={() => setIsOpenIntroduce(true)}>
        <CustomIcon icon={introduce} />
        <span className="menu-title">{t('header.introduce')}</span>
      </Menu.Item>
      <Menu.Item key="2" onClick={() => changeLanguage('vi')}>
        <CustomIcon icon={vietnamese} />
        <span className={`menu-title ${language === 'vi' && 'color-vn'}`}>{t('header.vietnamese')}</span>
      </Menu.Item>
      <Menu.Item key="3" onClick={() => changeLanguage('en')}>
        <CustomIcon icon={english} />
        <span className={`menu-title ${language === 'en' && 'color-vn'}`}>{t('header.english')}</span>
      </Menu.Item>
      <Menu.Item key="4" onClick={() => setIsOpenChangePassword(true)}>
        <CustomIcon icon={changePassword} />
        <span className="menu-title">{t('header.changePassword')}</span>
      </Menu.Item>
      <Menu.Item key="5" onClick={() => setIsOpenSettings(true)}>
        <CustomIcon icon={settings} />
        <span className="menu-title">{t('header.settings')}</span>
      </Menu.Item>
      <Menu.Item key="6" onClick={() => setIsOpenLogout(true)}>
        <CustomIcon icon={logout} />
        <span className="menu-title">{t('header.logout')}</span>
      </Menu.Item>
    </Menu>
  );
  const menuNotify = (
    <Menu className="notify">
      {[1, 2].map((item) => (
        <Menu.Item key={item}>
          <div>
            <div className="noti-header">Camera 001: Ngã tư Nguyễn Hữu Dực - Lê Đức Thọ</div>
            <div className="noti-content">
              <div className="image">
                <img src={Rectangle} alt="anh minh hoa" />
              </div>
              <div className="content">Không đeo khẩu trang: 1 người</div>
            </div>
            <div className="noti-action">
              <CustomButton text="Xử lý cảnh báo" />
            </div>
          </div>
        </Menu.Item>
      ))}
    </Menu>
  );
  const handleDrawerOpen = () => {
    dispatch(layoutActions.isOpenSidebar(true));
  };
  const handleDrawerClose = () => {
    dispatch(layoutActions.isOpenSidebar(false));
  };
  const onFullScreen = () => {
    if (isFullScreen) {
      dispatch(headerActions.exitFullScreen());
    } else {
      dispatch(headerActions.isFullScreen());
    }
  };
  return (
    <Header className="header-layout-background">
      <div className="icon-action">
        {isOpen ? (
          <button onClick={handleDrawerClose} type="button" className="button-open-close">
            <img src={closeSideBar} className="trigger-open-close" alt="" />
          </button>
        ) : (
          <button onClick={handleDrawerOpen} type="button" className="button-open-close">
            <img src={openSideBar} className="trigger-open-close" alt="" />
          </button>
        )}
      </div>
      <div className="feature">
        <Space wrap>
          <Dropdown
            overlay={menu}
            trigger={['click']}
            placement="bottomRight"
            getPopupContainer={(trigger) => trigger.parentNode}
          >
            <Button className="custom-btn">
              <CustomIcon icon={infoUser} />
              <span>User Login</span>
              <CustomIcon icon={dropdown} />
            </Button>
          </Dropdown>
          {isShowMic ? (
            <Button className="custom-btn" onClick={() => checkPermissionsMic()}>
              <CustomIcon icon={mic} />
            </Button>
          ) : null}
          {isShowFullScreen && (
            <Button className="custom-btn" onClick={() => onFullScreen()}>
              <CustomIcon icon={!isFullScreen ? full : noFull} />
            </Button>
          )}
          <Dropdown overlay={menuNotify} trigger={['click']} getPopupContainer={(trigger) => trigger.parentNode}>
            <Button className="custom-btn">
              <CustomIcon icon={bell} />
            </Button>
          </Dropdown>
          <Button className="custom-btn">{time}</Button>
        </Space>
      </div>
      <ModalIntroduce isOpen={isOpenIntroduce} onClose={() => setIsOpenIntroduce(false)} />
      <ModalChangePassword
        isOpen={isOpenChangePassword}
        onClose={() => {
          setIsOpenChangePassword(false);
        }}
      />
      <ModalSettings isOpen={isOpenSettings} onClose={() => setIsOpenSettings(false)} />
      <ModalLogout isOpen={isOpenLogout} onClose={() => setIsOpenLogout(false)} />
    </Header>
  );
};

Headers.propTypes = {};

export default Headers;
