import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import CustomModal from '../../common/CustomModal/CustomModal';
import CustomButton from '../../common/CustomButton/CustomButton';

import './style.scss';

const ModalIntroduce = (props) => {
  const { t } = useTranslation();
  const { isOpen, onClose } = props;

  return (
    <CustomModal
      width={352}
      title={t('introduce.title')}
      visible={isOpen}
      onCancel={onClose}
      footer={(
        <div className="action-modal">
          <CustomButton onClick={onClose} text="Thoát" />
        </div>
      )}
    >
      <div className="content-modal">
        <p>
          {t('introduce.version')}
          : 1.15
        </p>
        <p>Bản quyền thuộc về Tổng công ty giải pháp doanh nghiệp Viettel</p>
      </div>
    </CustomModal>
  );
};

ModalIntroduce.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ModalIntroduce;
