import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import CustomModal from '../../common/CustomModal/CustomModal';
import CustomButton from '../../common/CustomButton/CustomButton';

import './style.scss';

const ModalLogout = (props) => {
  const { t } = useTranslation();
  const history = useHistory();

  const { isOpen, onClose } = props;

  const onLogout = () => {
    history.push('/login');
  };

  return (
    <CustomModal
      width={352}
      title={t('logout.title')}
      visible={isOpen}
      onCancel={onClose}
      footer={(
        <div className="action-modal">
          <CustomButton className="cancel" onClick={onClose} text="Huỷ bỏ" />
          <CustomButton onClick={onLogout} text="Xác nhận" />
        </div>
      )}
    >
      <div className="content-modal">
        <div className="content-notify">Bạn có chắc chắn muốn Đăng xuất khỏi hệ thống?</div>
      </div>
    </CustomModal>
  );
};

ModalLogout.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ModalLogout;
