import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import { Form, Input } from 'antd';
import CustomModal from '../../common/CustomModal/CustomModal';
import CustomButton from '../../common/CustomButton/CustomButton';
import Label from '../../common/CustomLabel/CustomLabel';
import { checkFormatPassword } from '../../../utils/function';
import './style.scss';

const ModalChangePassword = (props) => {
  const { t } = useTranslation();
  const { isOpen, onClose } = props;
  const [form] = Form.useForm();

  const onSubmitForm = () => {
    form.validateFields().then(() => {});
  };

  useEffect(() => () => form.resetFields(), [isOpen, form]);

  return (
    <CustomModal
      width={352}
      title={t('changePass.title')}
      visible={isOpen}
      onCancel={onClose}
      footer={(
        <div className="action-modal">
          <CustomButton className="cancel" onClick={onClose} text="Huỷ bỏ" />
          <CustomButton onClick={onSubmitForm} text="Đổi mật khẩu" />
        </div>
      )}
    >
      <div className="content-modal">
        <Form form={form} layout="vertical" autoComplete="off">
          <Label title="Mật khẩu hiện tại" isRequire />
          <Form.Item
            label=""
            name="oldPassword"
            rules={[
              { required: true, message: 'Mật khẩu hiện tại không được để trống!' },
              () => ({
                validator(_, value) {
                  if (value && !checkFormatPassword(value)) {
                    return Promise.reject(
                      new Error(
                        'Nhập tối thiểu 8 ký tự bao gồm: Chữ, số, và ký tự viết HOA, viết thường, ký tự đặc biệt!',
                      ),
                    );
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Label title="Mật khẩu mới" isRequire />
          <Form.Item
            label=""
            name="password"
            rules={[
              { required: true, message: 'Mật khẩu mới không được để trống!' },
              () => ({
                validator(_, value) {
                  if (value && !checkFormatPassword(value)) {
                    return Promise.reject(
                      new Error(
                        'Nhập tối thiểu 8 ký tự bao gồm: Chữ, số, và ký tự viết HOA, viết thường, ký tự đặc biệt!',
                      ),
                    );
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Label title="Nhập lại mật khẩu mới" isRequire />
          <Form.Item
            label=""
            name="confirmPassword"
            dependencies={['password']}
            rules={[
              { required: true, message: 'Nhập lại mật khẩu mới không được để trống!' },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (value && getFieldValue('password') !== value) {
                    return Promise.reject(new Error('Nhập lại mật khẩu mới không trùng khớp với Mật khẩu mới!'));
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>
        </Form>
      </div>
    </CustomModal>
  );
};

ModalChangePassword.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ModalChangePassword;
