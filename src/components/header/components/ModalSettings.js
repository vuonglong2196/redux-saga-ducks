import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import {
  Form, Radio, Space, Divider,
} from 'antd';
import CustomModal from '../../common/CustomModal/CustomModal';
import CustomButton from '../../common/CustomButton/CustomButton';
import CustomRadio from '../../common/CustomRadio/CustomRadio';
import CustomCheckbox from '../../common/CustomCheckbox/CustomCheckbox';
import CustomSelect from '../../common/CustomSelect/CustomSelect';

import './style.scss';

const ModalSettings = (props) => {
  const { t } = useTranslation();
  const { isOpen, onClose } = props;
  const [form] = Form.useForm();

  const lstOptionNotify = [
    { title: 'Email', value: 1 },
    { title: 'SMS', value: 2 },
  ];

  const lstOptionOTP = [
    { title: 'Email', value: 1 },
    { title: 'SMS', value: 2 },
    { title: 'Google authenticator', value: 3 },
  ];

  const onSubmitForm = () => {
    form.validateFields().then(() => {});
  };

  useEffect(() => () => form.resetFields(), [isOpen, form]);

  return (
    <CustomModal
      width={352}
      title={t('settings.title')}
      visible={isOpen}
      onCancel={onClose}
      footer={(
        <div className="action-modal">
          <CustomButton className="cancel" onClick={onClose} text="Huỷ bỏ" />
          <CustomButton onClick={onSubmitForm} text="Lưu lại" />
        </div>
      )}
    >
      <div className="content-modal">
        <Form
          form={form}
          layout="vertical"
          initialValues={{
            mode: 2,
            isNotify: false,
            methodNotify: 1,
            isOTP: false,
            methodOTP: 1,
          }}
          autoComplete="off"
        >
          <Form.Item label="" name="mode">
            <Radio.Group>
              <Space direction="vertical">
                <CustomRadio value={1} title="Chế độ thả xuống" />
                <CustomRadio value={2} title="Chế độ cố định" />
              </Space>
            </Radio.Group>
          </Form.Item>
          <Divider />
          <Form.Item label="" name="isNotify" valuePropName="checked" className="mb-style">
            <CustomCheckbox label="Thông báo khi có thiết bị mới đăng nhập" />
          </Form.Item>
          <Form.Item noStyle shouldUpdate={(pre, curr) => pre.isNotify !== curr.isNotify}>
            {({ getFieldValue }) => (getFieldValue('isNotify') === true ? (
              <Form.Item label="Nhận thông báo qua" name="methodNotify">
                <CustomSelect placeholder="Chọn hình thức nhận thông báo" lstOption={lstOptionNotify} />
              </Form.Item>
            ) : null)}
          </Form.Item>
          <Divider />
          <Form.Item label="" name="isOTP" valuePropName="checked" className="mb-style">
            <CustomCheckbox label="Xác thực 2 lớp" />
          </Form.Item>
          <Form.Item noStyle shouldUpdate={(pre, curr) => pre.isOTP !== curr.isOTP}>
            {({ getFieldValue }) => (getFieldValue('isOTP') === true ? (
              <Form.Item label="Nhận OTP qua" name="methodOTP">
                <CustomSelect placeholder="Chọn hình thức nhận thông báo" lstOption={lstOptionOTP} />
              </Form.Item>
            ) : null)}
          </Form.Item>
        </Form>
      </div>
    </CustomModal>
  );
};

ModalSettings.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ModalSettings;
