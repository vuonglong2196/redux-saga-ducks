import axios from 'axios';
import { message } from 'antd';

const instance = axios.create({
  baseURL: `${window.SystemConfig.URI}`,
});

instance.defaults.timeout = 25000;

instance.interceptors.request.use(req => {
  const token = 'token';
  if (token) req.headers.Authorization = `Bearer ${token}`;
  return req;
});

instance.interceptors.response.use(
  response => response,
  error => {
    const responseError = {
      ...error,
      response: {
        ...error.response,
        // data: { message: "Can't connect to sever", ...error.response.data },
      },
    };

    message.error({
      content: `${responseError.response.data.message ||
        responseError.response.data ||
        "Can't connect to sever"} `,
      key: `showError`,
      duration: 3,
    });

    if (error.response.status === 401) {
      // Delete token and user
    }

    return responseError;
  },
);

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
 function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response.data;
  }

  const { data, status } = response.response;
  const error = new Error(response.statusText);
  error.response = {
    status,
    ...response,
    // data: { message: "Can't connect to sever", ...data },
  };
  throw error;
}

export async function axiosGet(path, params) {
  const res = await instance
    .get(path, { params })
    .then(checkStatus)
    .catch(error => {
      if (!JSON.parse(JSON.stringify(error)).response)
        message.error({
          content: "Can't connect to sever",
          key: `INTERNET_DISCONNECTED`,
          duration: 3,
        });
      throw error;
    });
  return res;
}
export async function axiosPost(path, body) {
  const res = await instance
    .post(path, body)
    .then(checkStatus)
    .catch(error => {
      if (!JSON.parse(JSON.stringify(error)).response)
        message.error({
          content: "Can't connect to sever",
          key: `INTERNET_DISCONNECTED`,
          duration: 3,
        });
      throw error;
    });
  return res;
}

export async function axiosPut(path, body) {
  const res = await instance
    .put(path, body)
    .then(checkStatus)
    .catch(error => {
      if (!JSON.parse(JSON.stringify(error)).response)
        message.error({
          content: "Can't connect to sever",
          key: `INTERNET_DISCONNECTED`,
          duration: 3,
        });
      throw error;
    });
  return res;
}

export async function axiosDelete(path, body) {
  const res = await instance
    .delete(path, { data: { ...body } })
    .then(checkStatus)
    .catch(error => {
      if (!JSON.parse(JSON.stringify(error)).response)
        message.error({
          content: "Can't connect to sever",
          key: `INTERNET_DISCONNECTED`,
          duration: 3,
        });
      throw error;
    });
  return res;
}
