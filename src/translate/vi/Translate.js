const TRANSLATIONS_VI = {
  login: {
    title: 'Đăng nhập hệ thống',
    username: 'Tên đăng nhập',
    password: 'Mật khẩu',
    usernameBlank: 'Tên đăng nhập không được để trống',
    passwordBlank: 'Mật khẩu không được để trống',
    forgotPassword: 'Quên mật khẩu?',
    remember: 'Ghi nhớ',
    submit: 'Đăng nhập',
    downloadPc: 'Tải phiên bản dành cho PC',
  },
  forgotPassword: {
    title: 'Tạo mật khẩu mới',
    username: 'Tên đăng nhập',
    newPassword: 'Mật khẩu mới',
    confirmNewPassword: 'Nhập lại mật khẩu mới',
    passwordBlank: 'Mật khẩu không được để trống',
    submit: 'Lưu thay đổi',
    email: 'Email đăng kí',
    notValidEmail: 'Email không đúng định dạng',
    confirmBtn: 'Xác nhận',
    forgot: 'Forgot password',
    emailBlank: 'Email không được để trống',
  },
  confirmOtp: {
    title: 'Nhập OTP',
    label: 'Nhập mã xác nhận',
    codeBlank: 'Mã xác nhận không được để trống',
    confirmBtn: 'Xác nhận',
    resend: 'Gửi lại',
  },
  header: {
    introduce: 'Giới thiệu',
    vietnamese: 'Tiếng Việt',
    english: 'Tiếng Anh',
    changePassword: 'Đổi mật khẩu',
    settings: 'Cài đặt',
    logout: 'Đăng xuất',
  },
  introduce: {
    title: 'Giới thiệu',
    version: 'Thông tin phiên bản',
  },
  changePass: {
    title: 'Đổi mật khẩu',
    oldPass: 'Mật khẩu cũ',
  },
  settings: {
    title: 'Cài đặt',
  },
  logout: {
    title: 'Đăng xuất',
  },

  // Face Recognition Management
  faceRecognitionManagement: {},
  registerFaceRecognition: {
    title: 'Đăng ký nhận diện khuôn mặt',
  },
};

export default TRANSLATIONS_VI;
