const TRANSLATIONS_EN = {
  login: {
    title: 'Login system',
    username: 'Username',
    password: 'Password',
    usernameBlank: 'Username must not be blank',
    passwordBlank: 'Password must not be blank',
    forgotPassword: 'Forgot password?',
    remember: 'Remember me',
    submit: 'Login',
    downloadPc: 'Download the desktop app',
  },
  forgotPassword: {
    title: 'Forgot password',
    username: 'Username',
    newPassword: 'New password',
    confirmNewPassword: 'Rewrite new password',
    passwordBlank: 'Password must not be blank',
    submit: 'Submit',
    email: 'Email',
    notValidEmail: 'The email is not valid',
    confirmBtn: 'Submit',
    forgot: 'Quên mật khẩu',
    emailBlank: 'Email must not be blank',
  },
  confirmOtp: {
    title: 'Confirm OTP',
    label: 'Type code',
    codeBlank: 'Code must not be blank',
    confirmBtn: 'Submit',
    resend: 'Resend',
  },
  header: {
    introduce: 'Introduce',
    vietnamese: 'Vietnamese',
    english: 'English',
    changePassword: 'Change Password',
    settings: 'Settings',
    logout: 'Logout',
  },
  introduce: {
    title: 'Introduce',
    version: 'Version',
  },
  changePass: {
    title: 'Change Password',
    oldPass: 'Old password',
  },
  settings: {
    title: 'Settings',
  },
  logout: {
    title: 'Logout',
  },

  // Face Recognition Management
  faceRecognitionManagement: {},
  registerFaceRecognition: {
    title: 'Register Face Recognition',
  },
};

export default TRANSLATIONS_EN;
