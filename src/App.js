import React, { useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from './routes/Routers';
import { selectIsFullScreen, selectExitFullScreen } from './redux/ducks/header/selectors'
import 'antd/dist/antd.css';
import './App.scss';

function App() {
  const child = useRef(null);
  const isFullScreen = useSelector(selectIsFullScreen());
  const exitFullScreen = useSelector(selectExitFullScreen());

  const launchFullscreen = () => {
    if (child.current.requestFullscreen) {
      child.current.requestFullscreen();
    } else if (child.current.mozRequestFullScreen) {
      child.current.mozRequestFullScreen();
    } else if (child.current.webkitRequestFullscreen) {
      child.current.webkitRequestFullscreen();
    } else if (child.current.msRequestFullscreen) {
      child.current.msRequestFullscreen();
    }
  };

  const exitFullscreen = () => {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen();
    }
  };

  useEffect(() => {
    if (isFullScreen) {
      launchFullscreen();
    }
    if (exitFullScreen && child.current) {
      exitFullscreen();
    }
  }, [isFullScreen, exitFullScreen]);

  return (
    <div ref={child}>
      <Router>
        <Routes />
      </Router>
    </div>
  );
}

export default App;
